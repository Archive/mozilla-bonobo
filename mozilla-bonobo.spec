Summary: Mozilla plugin for using bonobo components.
Name: mozilla-bonobo
Version: 0.4.2.1
Release: 0
License: GPL
Group: Applications/Internet
URL: http://www.nongnu.org/moz-bonobo/

Packager: Dag Wieers <dag@wieers.com>
Vendor: Dag Apt Repository, http://dag.wieers.com/home-made/apt/

Source:http://savannah.nongnu.org/download/moz-bonobo/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/root-%{name}-%{version}
Prefix: %{_prefix}

BuildRequires: gtk2-devel >= 2.0, glib2-devel >= 2.0, pango-devel >= 1.0.0
BuildRequires: atk-devel >= 1.0, freetype-devel >= 2.0, mozilla-devel >= 1.0
BuildRequires: mozilla-nspr-devel >= 1.0

%description
This package contains a plugin for the Mozilla browser that makes it
possible to use bonobo components.

%prep
%setup

%build
%configure \
	--with-plugin-install-dir="%{buildroot}%{_libdir}/mozilla/plugins" \
	--disable-schemas-install
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL="1"
%makeinstall

### Clean up buildroot
%{__rm} -rf %{buildroot}%{_prefix}/doc/

%clean
%{__rm} -rf %{buildroot}

%post
export GCONF_CONFIG_SOURCE="$(gconftool-2 --get-default-source)"
gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/%{name}.schemas &>/dev/null
scrollkeeper-update -q

%postun
scrollkeeper-update -q

%files
%defattr(-, root, root, 0755)
%config %{_sysconfdir}/gconf/schemas/*.schemas
%doc AUTHORS ChangeLog COPYING README NEWS TODO
%{_bindir}/*
%{_libdir}/mozilla/plugins/*.so

%changelog
* Thu May 08 2003 Dag Wieers <dag@wieers.com> - 0.1.0-0
- Initial package. (using DAR)
