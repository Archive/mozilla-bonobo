/*
 * Mozilla-bonobo plugin
 *
 * mozilla-bonobo-viewer.c
 *
 * Copyright (C) 2003
 *
 * Developed by Christian Glodt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
 * USA
 */

#include <gnome.h>
#include <gconf/gconf-client.h>
#include <glib.h>
#include <glib/gslist.h>
#include <bonobo.h>
#include <gdk/gdkx.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomeprint/gnome-print.h>
#include <string.h>
#include <stdio.h>
#include <bonobo-activation/bonobo-activation.h>

/* Function for printing debugging messages */
void DEBUGM(const char* format, ...) {
#ifdef DEBUG
	va_list args;
	va_start(args, format);	
	g_printerr(format, args);
	va_end(args);
#endif
}

/* If this define is set, users can blacklist mime types when components fail */
#undef BLACKLIST_UNSUPPORTED_MIME_TYPES
/*#define BLACKLIST_UNSUPPORTED_MIME_TYPES */

/* The URL of the document. *Not* used except for display. */
char *url;

/* The mime type of the document. */
char *mime_type;

/* The io channel through which the viewer receives commands from the plugin */
GIOChannel *in_channel;

/* The io channel through which the viewer sends commands to the plugin */
GIOChannel *out_channel;

/* The GtkPlug that embeds the viewer into the browser window */
GtkWidget *plug;	/* Main "window" */

/* The GtkFixed that is used to get sane resizing */
GtkWidget *fixed;

/* The progress bar that's shown during the download phase */
GtkWidget *progress;

/* The identifier of the timeout that's used to update the activity bar */
guint timeout;

gboolean timeout_stop = FALSE;

GtkWidget *widget;	/* The bonobo control's widget */
GtkWidget *control;
Bonobo_PropertyBag prop_bag;

/* The parent window of the plugin (ie the "socket") that we
   got passed as an XID from the plugin */
GdkWindow *gdk_parent;

/* The GConfClient used for GConf access :) */
GConfClient *gconfclient;

/* TRUE if in embedded mode, FALSE if standalone */
int embedded;

/* The size that we got passed from the plugin */
int width, height;

/* The control that is currently displayed.
   NOTE: This must only be set using the setMainControl function!!! */
GtkWidget *actual_control = NULL;

/* These functions are declared here because they are missing from the
   gnome-vfs include files.*/
GList *gnome_vfs_get_registered_mime_types();
GList *gnome_vfs_mime_get_extensions_list(const char*);
void gnome_vfs_mime_registered_mime_type_list_free(GList *);
const char* gnome_vfs_get_mime_type_from_file_data(GnomeVFSURI *uri);

Bonobo_ServerInfo *server_info;

/*this callback is called when the BonoboEventSource associated to the control emits
an event*/

static void
listener_callback (BonoboListener *l, char *event_name,
                      BonoboArg* value, CORBA_Environment *ev,
                      gpointer user_data)
{
	gsize nb;
	gchar* buf;
	buf = g_strdup_printf("%s\n%s\n", event_name, BONOBO_ARG_GET_STRING(value));
	g_io_channel_write (out_channel, buf, strlen(buf), &nb);
	g_free (buf);
}

/* This function creates a bonobo control that can display the given
 * URL. It sets the control to be non-editable, and returns NULL if
 * no control could be created.
 */
CORBA_Object obj;
GtkWidget *createBonoboControl(const char *url) {
	BonoboControlFrame *control_frame;
	
	CORBA_Environment ev;
	Bonobo_Control bcontrol;
	Bonobo_Unknown event_source;
	BonoboListener *listener;
	Bonobo_PersistStream ps;
	Bonobo_PersistFile f;
	
	server_info = gnome_vfs_mime_get_default_component(mime_type);
	
	CORBA_exception_init(&ev);
		
	obj = bonobo_activation_activate_from_id(server_info->iid, 0, NULL, &ev);
	if (BONOBO_EX(&ev)) {
		char *text = bonobo_exception_get_text(&ev);
		printf("Exception at activation: %s\n", text);
		g_free(text);
		CORBA_exception_free(&ev);
		return NULL;
	} else {
		CORBA_exception_free(&ev);
		CORBA_exception_init(&ev);
	}
	
	control = bonobo_widget_new_control(server_info->iid, CORBA_OBJECT_NIL);
	
	control_frame = bonobo_widget_get_control_frame(BONOBO_WIDGET(control));
	
	bcontrol = bonobo_control_frame_get_control(control_frame);

	ps = Bonobo_Unknown_queryInterface(bcontrol, "IDL:Bonobo/PersistStream:1.0", &ev);

	if (!BONOBO_EX(&ev)) {
		Bonobo_Stream s;
		
		CORBA_exception_free(&ev);
		CORBA_exception_init(&ev);
		
		s = bonobo_get_object(url, "IDL:Bonobo/Stream:1.0", &ev);
		if (BONOBO_EX(&ev)) {
			char *text = bonobo_exception_get_text(&ev);
			fprintf(stderr, "Exception at getObject(stream): %s\n", text);
			g_free(text);
		} else {

			Bonobo_PersistStream_load(ps, s, mime_type, &ev);
	
			if (BONOBO_EX(&ev)) {
				char *text = bonobo_exception_get_text(&ev);
				fprintf(stderr, "Exception at PersistStream_load: %s\n", text);
				g_free(text);
			}	
		}
	}
	if (BONOBO_EX(&ev)) {
		CORBA_exception_free(&ev);
		CORBA_exception_init(&ev);
		
		f = Bonobo_Unknown_queryInterface(bcontrol, "IDL:Bonobo/PersistFile:1.0", &ev);
		if (BONOBO_EX(&ev)) {
			char *text = bonobo_exception_get_text(&ev);
			printf("Exception at queryInterface(File): %s\n", text);
			g_free(text);
			
			control = bonobo_widget_new_control(url, CORBA_OBJECT_NIL);
			
			CORBA_exception_free(&ev);
			CORBA_exception_init(&ev);
		} else {
			Bonobo_PersistFile_load(f, url, &ev);
		}
	}

	if (BONOBO_EX(&ev)) {
		char *text = bonobo_exception_get_text(&ev);
		printf("Exception at load: %s\n", text);
		g_free(text);
		CORBA_exception_free(&ev);
		return NULL;
	} else {
		CORBA_exception_free(&ev);
		CORBA_exception_init(&ev);
	}
	
	CORBA_exception_free(&ev);
		
	if (control == NULL) return NULL;
	
	/* Get the control frame */
	control_frame = bonobo_widget_get_control_frame(BONOBO_WIDGET(control));
	if (control_frame) {
		
		/* Get property bag */
		prop_bag = bonobo_control_frame_get_control_property_bag (control_frame, NULL);
		if (prop_bag != CORBA_OBJECT_NIL) {
			/* Set editable property */
			bonobo_pbclient_set_boolean(prop_bag, "bonobo:editable", FALSE, NULL);
			DEBUGM("viewer: set editable to false\n");
		}
	}

	event_source = Bonobo_Unknown_queryInterface(bonobo_widget_get_objref (BONOBO_WIDGET(control)),
																					"IDL:Bonobo/EventSource:1.0", NULL);	
	if (event_source) {
		listener = bonobo_listener_new ((BonoboListenerCallbackFn) listener_callback, NULL);
		Bonobo_EventSource_addListener (event_source, bonobo_object_corba_objref (BONOBO_OBJECT(listener)), NULL);
	}

	return control;
}

GtkWidget *createPlug(unsigned long xid) {
	GtkWidget *res;
	
	/* This plugs into the browser */
	res = gtk_plug_new(xid);    

	/* Get the GdkWindow too */
	gdk_parent = gdk_window_foreign_new(xid);
	
	/* Set the initial size */
	gdk_window_get_geometry(gdk_parent, NULL, NULL, &width, &height, NULL);

	gtk_window_set_default_size(GTK_WINDOW(res), width, height);

	/* Important: realize the widget so it gets its own X window */
	gtk_widget_realize(res);

	/* Do some reparenting black magic */
	XReparentWindow(GDK_WINDOW_XDISPLAY(res->window),
		GDK_WINDOW_XID(res->window),
		xid, 0, 0);
	XMapWindow(GDK_WINDOW_XDISPLAY(res->window),
		GDK_WINDOW_XID(res->window));

	return res;
}

/* This function is the only one that should be used to set the main
 * control! It removes any previously set control.
 */
void setMainControl(GtkWidget *w) {
	
	if (actual_control) {
		gtk_container_remove(GTK_CONTAINER(fixed), GTK_WIDGET(actual_control));
	}
	
	gtk_widget_set_usize(GTK_WIDGET(w), width, height);
	
	gtk_fixed_put(GTK_FIXED(fixed), GTK_WIDGET(w), 0, 0);
	
	gtk_widget_show(GTK_WIDGET(w));
	
	actual_control = w;
}

/* Callback called if the window is closed. Applicable only in standalone mode.
 */
static void window_destroyed (GtkWindow *window, char * data) {
	Bonobo_Unknown bu; 
	gsize nb;
	
	g_io_channel_write (out_channel, "exit\n", 5, &nb);
	bu = bonobo_widget_get_objref (BONOBO_WIDGET(control));
	Bonobo_Unknown_unref (bu, NULL);
	bonobo_main_quit();
}

#ifdef BLACKLIST_UNSUPPORTED_MIME_TYPES

/* This function pops up a dialog that asks the user if he wants to
 * blacklist a mime type for which the component failed to load.
 * If he chooses yes, the mime type is added to the GConf key.
 */
static void blacklist_mime_type(const char *type) {
	
	char *msg = g_strdup_printf("GNOME could not start a viewer to display the content of type \"%s\" "
			      "in your browser. Do you want GNOME to try again the next time you "
			      "open this type of content?\n\n Note: you may have to restart your browser for this setting to take effect.", type);
	
	GtkWidget *yes_no_dialog = gnome_message_box_new(
		msg,
		GNOME_MESSAGE_BOX_QUESTION,
		GNOME_STOCK_BUTTON_NO,
		GNOME_STOCK_BUTTON_YES,
		NULL);
		
	g_free(msg);

	/* Run the dialog */
	int ret = gnome_dialog_run(GNOME_DIALOG(yes_no_dialog));

	GSList *list;
	
	switch(ret) {
	case 1:
		/* Yes, the user wants us to try again the next time */
		break;
	case 0:
		/* No, the user wants the mime type blacklisted */
	
		/* Get the list of already blacklisted mime types */
		list = gconf_client_get_list(gconfclient,
				"/apps/mozilla-bonobo/mime-types/ignore",
				GCONF_VALUE_STRING, NULL);
	
		/* Check if it's already blacklisted */
		GSList *node = list;
		int found = 0;
		while(node) {
			if (strcmp(node->data, type) == 0) {
				found = 1;
				break;
			}
			node = node->next;
		}
	
		if (!found) {
			
			/* It was not yet blacklisted, append the mime type to the list */
			list = g_slist_append(list, g_strdup(type));
			
			/* Store the key */
			gconf_client_set_list(gconfclient, "/apps/mozilla-bonobo/mime-types/ignore",
			      GCONF_VALUE_STRING, list, NULL);

		}
		
		break;
	default:
		break;
	}
}

#endif /* BLACKLIST_UNSUPPORTED_MIME_TYPES */

/* This function is called as soon as data is available on the io channel
 * on which we receive commands from the plugin.
 */
static gboolean io_func(GIOChannel *source, GIOCondition condition, gpointer data) {
	GError *err = NULL;
	
	char *string;	/* The string we read from the io channel */

	gsize length;	/* Its size */
	
	/* Read 1 line from the io channel, including newline character */
	g_io_channel_read_line(source, &string, &length, NULL, &err);

	if (err != NULL) {
		/* Whoops, there's been an error */
		g_error("Error: %s\n", err->message);
		g_error_free(err);
		/* Try again next time */
		return TRUE;
	}
	
	/* Any of these conditions makes us ignore the string */
	if (length == 0) return TRUE;
	if (string == 0) return TRUE;
	
	DEBUGM("viewer: io_func(%s)", string);

	if (g_str_equal("size\n", string) == TRUE) {
		/* The command was "size" */
		int w, h;
		
		g_free(string);
		
		/* Read the new width */
		g_io_channel_read_line(source, &string, &length, NULL, NULL);
		w = atoi(string);
		g_free(string);
		
		/* Read the new height */
		g_io_channel_read_line(source, &string, &length, NULL, NULL);
		h = atoi(string);
		g_free(string);
		
		DEBUGM(" %i, %i", w, h);
		
		/* If the size actually changed */
		if (w != width || h != height) {
		
			width = w;
			height = h;
			
			/* Set it */
			gtk_widget_set_usize(GTK_WIDGET(plug), w, h);
			gtk_widget_set_usize(GTK_WIDGET(actual_control), w, h);
			
			/* If the parent is known, try to resize it */
			if (gdk_parent) {
				gdk_window_resize(GDK_WINDOW(gdk_parent), w, h);
			}
		}
		return TRUE;
 	}

	if (g_str_equal("filename\n", string) == TRUE) {
		/* The command was "filename". This command indicates that
		   downloading of the file is finished, and passes us the
		   "file:///" url of the file in mozillas cache */
		
		char *stripped_name;
		
		/* In any case, make the timeout stop next time */
		timeout_stop = TRUE;
		
		g_free(string);
		
		/* Read the file URL */
		g_io_channel_read_line(source, &string, &length, NULL, NULL);
		
		/* Strip off the newline */
		stripped_name = g_strstrip(string);
		
		DEBUGM(" %s", stripped_name);

		/* Create a control that can display the URL */
		control = createBonoboControl(stripped_name);

		if (control == NULL) {
			/* The control failed to load */
			char *msg;
						
			/* Set up the label */
			msg = g_strdup_printf("Couldn't load viewer for content of type \"%s\".", mime_type);
			control = gtk_label_new(msg);
			gtk_label_set_line_wrap(GTK_LABEL(control), TRUE);
			gtk_label_set_justify(GTK_LABEL(control), GTK_JUSTIFY_CENTER);
			g_free(msg);
			
			setMainControl(control);
	
#ifdef BLACKLIST_UNSUPPORTED_MIME_TYPES
			
			/* Ask the user if he wishes to blacklist the mime type */
			blacklist_mime_type(mime_type);
			
#endif /* BLACKLIST_UNSUPPORTED_MIME_TYPES */
			
		} else {
			/* The control got correctly instantiated */

			setMainControl(control);			
		}
		g_free(string);
		return TRUE;
	}
	
	if (g_str_equal("exit\n", string) == TRUE) {
		gsize nb;
		Bonobo_Unknown bu;
		/* The command was "exit", so that's what we do */
		
		g_free(string);
		
		g_io_channel_write (out_channel, "exit\n", 5, &nb);
		
		bu = bonobo_widget_get_objref (BONOBO_WIDGET(control));
		Bonobo_Unknown_unref (bu, NULL);
		bonobo_main_quit();
		return TRUE;
	}
	
	if (g_str_equal("param\n", string) == TRUE) {
		/* Getting a param to pass to the control */
		
		char *name, *end;
		CORBA_TypeCode tc;
		double d;
		int i;
		guint64 u;
		
		g_free(string);
		/* First, retrieve the param name */
		g_io_channel_read_line(source, &string, &length, NULL, NULL);
		name = g_strdup(string);
		name[strlen(name) - 1] = 0;/*to remove the \n*/
		g_free (string);
		
		/* Now, get the value */
		g_io_channel_read_line(source, &string, &length, NULL, NULL);
		string[strlen(string) - 1] = 0;/*to remove the \n*/
		
		if (prop_bag != CORBA_OBJECT_NIL) {
			if ((tc = bonobo_pbclient_get_type (prop_bag, name, NULL)) != CORBA_OBJECT_NIL)	{

				switch (tc->kind) {

					case CORBA_tk_string:
						bonobo_pbclient_set_string (prop_bag, name, string, NULL);
						break;
					case CORBA_tk_float:
						d = g_ascii_strtod (string, &end);
						if (!end) bonobo_pbclient_set_float (prop_bag, name, (float) d, NULL);
						else g_warning ("Error: bad float value %s\n", string);
						break;
					case CORBA_tk_double:
						d = g_ascii_strtod (string, &end);
						if (!end) bonobo_pbclient_set_float (prop_bag, name, d, NULL);
						else g_warning ("Error: bad double value %s\n", string);
							break;
					case CORBA_tk_short:
						i = atoi (string);
						bonobo_pbclient_set_short (prop_bag, name, (gint16) i, NULL);
						break;
					case CORBA_tk_long:
						i = atoi (string);
						bonobo_pbclient_set_long  (prop_bag, name, (gint32) i, NULL);
						break;
					case CORBA_tk_ushort:
						u = g_ascii_strtoull (string, &end, 10);
						if (!end) bonobo_pbclient_set_ushort (prop_bag, name, (guint16)u, NULL);
						else g_warning ("Error: bad unsigned short value %s\n", string);
						break;
					case CORBA_tk_ulong:
						u = g_ascii_strtoull (string, &end, 10);
						if (!end) bonobo_pbclient_set_ulong (prop_bag, name, (guint32)u, NULL);
						else g_warning ("Error: bad unsigned long value %s\n", string);
						break;
					default:
						g_warning ("Error: unsupported type\n");
				}
			}
		}
		g_free (name);
		
		return TRUE;
	}

	if (g_str_equal("reparent\n", string) == TRUE) {
		/* The command was reparent */
		unsigned long new_xid;
		GtkWidget *dialog;
		int ret;
		
		g_free(string);
		
		/* Read the new XID */
		g_io_channel_read_line(source, &string, &length, NULL, NULL);
		
		/* Parse the XID */
		new_xid = strtoul(string, NULL, 0);
		g_free(string);

		dialog = gnome_message_box_new(
			"The document viewer was unable to reparent its window in "
			"response to your browser's operation. The document viewer "
			"will be shut down.",
			GNOME_MESSAGE_BOX_ERROR,
			GNOME_STOCK_BUTTON_OK,
			NULL);

		/* Run the dialog */
		ret = gnome_dialog_run(GNOME_DIALOG(dialog));

		bonobo_main_quit();
		
		return TRUE;
	}

	if (g_str_equal("print_embedded\n", string) == TRUE) {
		/* The command was "print_embedded", so that's what we do */

		/* Note: This is test code which does not work yet */
	
		Bonobo_Stream res;
		Bonobo_PrintDimensions pd;		
		Bonobo_PrintScissor ps;
		CORBA_Environment ev;
		
		g_free(string);		
		
		ps.width_first_page = 10;
		ps.width_per_page = 10;
		ps.height_first_page = 10;
		ps.height_per_page = 10;
		
		pd.width = 10;
		pd.height = 10;

		CORBA_exception_init (&ev);
		
		res = Bonobo_Print_render(bonobo_widget_get_objref(BONOBO_WIDGET(actual_control)),
					  &pd, &ps, &ev);

		if (BONOBO_EX(&ev)) {
			char *text = bonobo_exception_get_text(&ev);
			printf("Exception: %s\n", text);
			g_free(text);
		}

		return TRUE;
	}

	if (g_str_equal("print_fullpage\n", string) == TRUE) {
		/* The command was "print_fullpage", so that's what we do */

		g_free(string);

		/* Not yet implemented */

		/* g_print("3\nXYZ"); */
		
		return TRUE;
	}
	
	/* At last, free the string */
	g_free(string);
	
	DEBUGM("\n");
	return TRUE;
}

/* This function is called if the plugin specifies the "--list-mime-types"
 * parameter to query the supported mime types. The types are returned in
 * a string that's formatted in a special way.
 */
char* getmimes() {
	
	GString *res = NULL;
	char *cres;
	int i, j;
	Bonobo_ServerInfoList *servers;
	
	/* This hashtable contains the mime types which are already in the list.
	   They need to be memorized because several components might support the
	   same mime type, which must however be contained only once in the string. */
	GHashTable *hash;
	
	GSList *list = NULL;
	GSList *node = NULL;
	
	/* This list contains the pre-compiled GPatternSpec* entries */
	GSList *patternslist = NULL;
	
	DEBUGM("viewer: getmimes()\n");
	
	hash = g_hash_table_new(g_str_hash, g_str_equal);
	
	res = g_string_new("");

	/* Get the list of blacklisted mime types */
	list = gconf_client_get_list(gconfclient,
			"/apps/mozilla-bonobo/mime-types/ignore",
			GCONF_VALUE_STRING, NULL);

	/* For each blacklisted mime type */
	node = list;
	while(node) {
		/* Compile the mime type into a GPatternSpec (for wildcard support (*, ?)) */
		patternslist = g_slist_prepend(patternslist,
					       g_pattern_spec_new(node->data));
		node = node->next;
	}

	/* The OAF query. It returns all the servers which support Embeddable/Control
	   and at least one of PersistFile/PersistStream. */
	servers = bonobo_activation_query("repo_ids.has_one (['IDL:Bonobo/Embeddable:1.0', "
							     "'IDL:Bonobo/Control:1.0']) "
				      "AND repo_ids.has_one (['IDL:Bonobo/PersistFile:1.0', "
							     "'IDL:Bonobo/PersistStream:1.0'])",
					  NULL, NULL);

	/* For each server */
	for (i = 0; i < servers->_length; i++) {
		Bonobo_ServerInfo *server;
		Bonobo_StringList types_strings;
		Bonobo_ActivationProperty *types_prop;
		
		server = &servers->_buffer[i];
		
		/* Get its supported mime types */
		types_prop = bonobo_server_info_prop_find(server, "bonobo:supported_mime_types");
		
		/* None? Next patient please. */
		if (types_prop == NULL) continue;
		
		/* Get the string list from the property */
		types_strings = types_prop->v._u.value_stringv;
		
		/* For each string in the list */
		for (j = 0; j < types_strings._length; j++) {
			Bonobo_ServerInfo* default_server_info;		
			GList *extensions;
			const char *desc;
			const char *comp_name;
			int is_ignored;
			
			/* Get the actual type */
			char *type = types_strings._buffer[j];

			/* Check all the patterns in patternslist */
			node = patternslist;
			
			is_ignored = FALSE;
			while(node) {
				
				/* If one matches, the type should be ignored */
				if (g_pattern_match_string(node->data, type)) {
					is_ignored = TRUE;
					break;
				}
				node = node->next;
			}
			
			if (is_ignored) continue;
				
			/* Get the server info for the default component */
			default_server_info = gnome_vfs_mime_get_default_component(type);
			
			/* If it's NULL, we interpret this as "there's no default component" */
			if (default_server_info == NULL) {
				continue;
			}
			
			/* See if we have already encountered the same mime type */
			if (g_hash_table_lookup(hash, type) != NULL) {
				/* If so, skip to the next type */
				continue;
			}

			/* Memorize that we have treated this mime type */
			g_hash_table_insert(hash, type, (gpointer)1);
			
			/* Append it to the string */
			g_string_append(res, type);
			g_string_append(res, ":");
			
			/* Get all the known extensions for the mime type */
			extensions = gnome_vfs_mime_get_extensions_list(type);
			if (extensions != 0) {
				do {
					/* Append them to the result string */
					g_string_append(res, extensions->data);
					if (extensions->next != NULL) {
						g_string_append(res, ",");
					}
				} while((extensions = extensions->next));				
			}
	
			g_string_append(res, ":");

			/* Get the mime type description and append it if available */
			desc = gnome_vfs_mime_get_description(type);
			
			if (desc != NULL) {
				g_string_append(res, desc);
			}
			
			/* Get the name of the default component */
			comp_name = bonobo_server_info_prop_lookup(default_server_info, "name", NULL);

			/* If the name is an actual string, append it */
			if ((comp_name != NULL) && (strlen(comp_name) > 0)) {
				g_string_append(res, " (using ");
				g_string_append(res, comp_name);
				g_string_append(res, ")");
			}
			
			/* That's it, we're done with this type */
			g_string_append(res, ";");
		}
	}
	
	/* Free the hash */
	g_hash_table_destroy(hash);
	
	/* and the servers */
	CORBA_free(servers);
	
	/* Return result */
	cres = g_string_free(res, FALSE);
	
	return cres;
}

/* This function updates the activity bar during the time the file is
 * downloaded. It's called by a glib timeout.
 */
gboolean timeout_func(gpointer data) {
	static int p = 0;

	/* The progress bar is not shown any longer, return FALSE
	   so the timeout won't be called again. */
	if (timeout_stop) return FALSE;
	
	/* Set some value */
	gtk_progress_set_value(GTK_PROGRESS(progress), p / 1000.0f);
	
	p = (p + 50) % 1000;

	/* Call this again please */
	return TRUE;
}

/* Main program entry point.
 */
int main(int argc, char * argv[]) {

	/* The XID of the socket we should plug into */
	unsigned long xid;
	
	GError *err = NULL;
	
	gdk_parent = NULL;
	control = NULL;
	width = 0;
	height = 0;
	
	/* See if the arguments are somewhat correct */
	if( argc <= 1 ) { 
		fprintf(stderr, "%s: not enough args\n", argv[0] );
		fprintf(stderr, "Usage: %s <url> <mime type> [xid]\n", argv[0]);
		fprintf(stderr, "       %s --list-mime-types\n", argv[0]);
		exit(1);
	}

	/* Init some stuff */
	bonobo_ui_init ("container", "1.0", &argc, argv);
	
	gconfclient = gconf_client_get_default();

	if (argc == 2 && g_str_equal(argv[1], "--list-mime-types")) {
		/* We're being invoked only to query the supported mime types */
		char *mimes = getmimes();
		
		/* Return through standard out */
		g_print("%s", mimes);
		g_free(mimes);
		return 0;
	}
	
	/* Set the url */
	url = argv[1];

	/* Set the Mime type */
	mime_type = argv[2];
	
	/* Init some more */
	if(gnome_vfs_init () == FALSE) {
		g_error (_("Could not initialize GnomeVFS!\n"));
	}
	
	if (argc == 4) {
		/* This is embedded mode */
    
		/* Parse the XID */
		xid = strtoul(argv[3], NULL, 0);
    
		plug = createPlug(xid);
		
		embedded = TRUE;
	} else {
		
		/* This is standalone mode */
		plug = gtk_window_new(GTK_WINDOW_TOPLEVEL);
		embedded = FALSE;
	}

	/* Connect some standard signals, so we exit cleanly */
	gtk_signal_connect(GTK_OBJECT(plug), "delete_event", GTK_SIGNAL_FUNC (window_destroyed), NULL);
	gtk_signal_connect(GTK_OBJECT(plug), "destroy", GTK_SIGNAL_FUNC(window_destroyed), NULL);
	
	if (!embedded) {
		/* In standalone mode, just try to load a control and display it */
		widget = createBonoboControl(url);
		
		if (widget == NULL) {
			/* If not, bad luck */
			widget = gtk_label_new(_("No bonobo viewer available!"));
		}
	} else {

		/* In embedded mode, prepare the progress bar, and align it */
		
		GtkWidget *vbox, *label, *alignment;
		char message[1024];
		memset(message, 0, 1024);
			
		widget = gtk_alignment_new(0.5f, 0.5f, 0.0f, 0.0f);
		
		alignment = gtk_alignment_new(0.5f, 0.5f, 0.0f, 0.0f);
		
		vbox = gtk_vbox_new(FALSE, 12);
		
		gtk_container_add(GTK_CONTAINER(widget), GTK_WIDGET(vbox));
				
		g_snprintf(message, 1023,_("Loading %s"), argv[1]);
		
		label = gtk_label_new(message);
		gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);
		gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
		gtk_container_add(GTK_CONTAINER(vbox), GTK_WIDGET(label));
		
		progress = gtk_progress_bar_new();
		
		gtk_progress_set_activity_mode(GTK_PROGRESS(progress), TRUE);
		
		gtk_container_add(GTK_CONTAINER(alignment), GTK_WIDGET(progress));
		gtk_container_add(GTK_CONTAINER(vbox), GTK_WIDGET(alignment));
	
		/* Set the timeout (20x per second), so that the activity
		   bar gets updated every once in a while. */
		timeout = g_timeout_add(50, timeout_func, 0);		
	}
	
	fixed = gtk_fixed_new();
	
	/* Whatever widget we constructed before, add and display it now */
	
	gtk_widget_set_usize(GTK_WIDGET(fixed), width, height);
	
	gtk_container_add(GTK_CONTAINER(plug), GTK_WIDGET(fixed));

	setMainControl(widget);

	if (!embedded) {
		
		gtk_widget_set_usize(GTK_WIDGET(plug), 400, 300);
	}
	
	gtk_widget_show_all(GTK_WIDGET(plug));		

	/* Connect the io channel through which we'll get commands */
	in_channel = g_io_channel_unix_new(fileno(stdin));	/* std in */
	g_io_add_watch(in_channel, G_IO_IN, io_func, &err);
	out_channel = g_io_channel_unix_new(fileno(stdout));	/* std in */
	
	if (err != NULL) {
		g_error("Error: %s\n", err->message);
		g_error_free(err);
	}

	bonobo_main();
	
	CORBA_free(server_info);

	return 0;
}
